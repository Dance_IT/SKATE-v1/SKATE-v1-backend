create table workspaces (
    id integer primary key autoincrement,
    name varchar unique not null,
    file_id varchar unique not null
);

create table files (
    id integer primary key autoincrement,
    name varchar not null,
    workspace_id integer not null,
    file_id varchar not null,
    unique (name, workspace_id),
    foreign key(workspace_id) references workspaces(id)
);

create table rounds (
    workspace_id integer not null,
    ckey varchar not null,
    stage integer not null,
    file_id varchar not null,
    participants_count integer not null,
    next_participants_count integer not null,
    heats_count integer not null,
    primary key (workspace_id, ckey, stage)
);