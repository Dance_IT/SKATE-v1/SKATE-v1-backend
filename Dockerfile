FROM python:3.12-slim AS builder

ENV POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_NO_INTERACTION=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

ENV PATH="$POETRY_HOME/bin:$PATH"

RUN apt-get update \
    && apt-get install -y --no-install-recommends curl \
    && curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /app

COPY poetry.lock pyproject.toml ./

RUN poetry install --no-root --no-ansi --without dev && rm -rf $POETRY_CACHE_DIR

FROM python:3.12-slim

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PATH="/app/.venv/bin:$PATH"

WORKDIR /app

COPY --from=builder /app/.venv ./.venv
COPY ./src ./

EXPOSE 80
ENV SKATE_CREATOR_EMAIL= \
    SECRET_KEY=

VOLUME [ "/data/gkey.json", "/data/main.db", "/data/workdir" ]

CMD ["env", "SKATE_WORKDIR=/data/workdir/", "SKATE_SERVICE_ACCOUNT_FILE=/data/gkey.json", "SKATE_DB_URI=sqlite:////data/main.db", "python", "-m", "flask", "--app", "skate_backend.app", "run", "--host=0.0.0.0", "--port=80"]
