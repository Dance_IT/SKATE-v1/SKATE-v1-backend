from skate_backend.utils import get_file_id_from_url


def test_get_file_id_from_url() -> None:
    file_id = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
    assert (
        get_file_id_from_url(f"https://docs.google.com/spreadsheets/d/{file_id}/edit")
        == file_id
    )
    assert (
        get_file_id_from_url(f"https://docs.google.com/spreadsheets/d/{file_id}/")
        == file_id
    )
    assert (
        get_file_id_from_url(f"https://docs.google.com/spreadsheets/d/{file_id}")
        == file_id
    )
    assert get_file_id_from_url(file_id) is None
