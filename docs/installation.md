# Установка

Установите [poetry](https://python-poetry.org/docs/#installation).
Далее выполните:
```
poetry install
poetry shell
```

## Получение токена для сервисного аккаунта

1. Зайти на https://console.cloud.google.com/
2. В левом верхнем углу (рядом с логотипом) выбрать проект или создать новый.
3. Включить у проекта работу с [Sheets API](https://console.cloud.google.com/apis/library/sheets.googleapis.com) и [Drive API](https://console.cloud.google.com/apis/library/drive.googleapis.com). **Важно**: если у вас несколько проектов, то перепроверьте, что включаете API в нужном проекте.
4. Перейдите в [окно создания сервисного аккаунта](https://console.cloud.google.com/iam-admin/serviceaccounts/create). Следуйте инструкциям. На втором шаге никаких ролей выдавать не надо. На третьем шаге тоже ничего не надо делать.
5. После этого появится сервисный аккаунт (список сервисных аккаунтов находится [тут](https://console.cloud.google.com/iam-admin/serviceaccounts)).
6. Нажать на email сервисного аккаунта, далее перейти во вкладку *Keys*. Далее нажать *Add key*, *Create new key*, *Create*.

По итогу будет скачан JSON файл, далее он называется `gkey.json`.

## Создание БД

Надо создать новую БД командой ([файлы миграции](../migrations)):
```
sqlite3 /path/to/file.db < migrations/0001.sql
```

## Запуск

Для запуска докер образа стоит выполнить команду, подобную той, что есть в [start_template.sh](../start_template.sh).
Перед началом нужно иметь созданную БД и `gkey.json`, а также нужно создать пустую папку (в дальнейшем `workdir`).
При запуске примонтировать их к докеру (`/data/main.db`, `/data/gkey.json` и `/data/workdir` соответственно).
Также нужно указать `SKATE_CREATOR_EMAIL` (гугловский email), чтобы иметь доступ к папкам, которые создаёт бот, и `SECRET_KEY` — любая последовательность символов (сейчас не так важно, так как используется локально, но указать всё равно надо).


Для запуска из исходнников выполните команду:
```
poetry run flask --app skate_backend.app run
```


## Ссылки

- [Разработка](./development.md)
