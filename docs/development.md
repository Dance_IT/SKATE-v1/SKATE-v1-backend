# Разработка

Проект следует проверить линтером и отформатировать:
```
make check
make format
```

Для тестирования:
```
poetry run pytest
```

## Получение доступа к Google API

Для простоты можно использовать [service account](https://cloud.google.com/iam/docs/service-account-overview).

1. Создать проект в [Google Cloud](https://console.cloud.google.com).
2. Перейти в **APIs & services** > **Credentials**.
3. Найти кнопку **Manage service accounts**.
4. Нажать на кнопку **Create service account** (сверху).
5. Проследовать процессу создания аккаунта.
6. Выберите роль *Editor* или *Owner*.
7. Далее нажать на email только что созданного аккаунта.
8. Перейти в меню **Keys**.
9. Нажать **Add key** > **Create new key**, проследовать инструкции.

По итогу должен быть скачан файл.

Далее нужно включить необходимые API:
1. [Google Drive API](https://console.cloud.google.com/apis/library/drive.googleapis.com).
2. [Google Sheets API](https://console.cloud.google.com/apis/library/sheets.googleapis.com).
