class ApplicationError(Exception):
    def __init__(self, title, message):
        self.title = title
        self.message = message

    def __str__(self):
        return self.message


class DBError(ApplicationError):
    def __init__(self, message):
        super().__init__("DB Error", message)


class GoogleAPIError(ApplicationError):
    def __init__(self, message):
        super().__init__("Google API Error", message)


class WrongDataFormatError(ApplicationError):
    def __init__(self, message):
        super().__init__("Wrong data format error", message)
