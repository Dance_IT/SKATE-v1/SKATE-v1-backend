from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request

from skate_backend import utils
from skate_backend.ctx import WorkspaceName
from skate_backend.exceptions import ApplicationError
from skate_backend.exceptions import WrongDataFormatError
from skate_backend.logic import reg_manager
from skate_backend.logic import round_manager
from skate_backend.logic.cat_manager import get_category_names
from skate_backend.logic.config_manager import load_categories_config
from skate_backend.logic.workspace_manager import RESERVED_FILE_NAMES
from skate_backend.logic.workspace_manager import create_workspace
from skate_backend.logic.workspace_manager import get_workspace_by_id
from skate_backend.logic.workspace_manager import list_workspaces


index = Blueprint("index", __name__)


@index.route("/")
def do_list_workspaces():
    workspaces = list_workspaces()
    return render_template("index.html", workspaces=workspaces)


@index.route("/workspaces", methods=["POST"])
def do_create_workspace():
    if "name" not in request.form or not request.form["name"].strip():
        return render_template("error.html", message="Нужно передать имя пространства")
    create_workspace(request.form["name"])
    return redirect("/")


def retrieve_workspace(workspace_id: str):
    try:
        workspace_id = int(workspace_id)
    except ValueError:
        raise ApplicationError("Неверный workspace id", "Неверный workspace id")
    workspace = get_workspace_by_id(workspace_id)
    if workspace is None:
        raise ApplicationError("Workspace does not exist", "Workspace does not exist")
    WorkspaceName.set(workspace.name)
    return workspace


@index.route("/workspaces/<workspace_id>", methods=["GET", "POST"])
def do_workspace(workspace_id: str):
    workspace = retrieve_workspace(workspace_id)
    if request.method == "POST":
        return do_workspace_post(workspace, request.form["action"])
    return do_workspace_get(workspace)


def do_workspace_get(workspace):
    files = workspace.files
    reserved = {x.name: x for x in files if x.name in RESERVED_FILE_NAMES}
    tournament_files = [x for x in files if x.name not in RESERVED_FILE_NAMES]
    return render_template(
        "workspace.html",
        workspace=workspace,
        reserved_files=reserved,
        tournamet_files=tournament_files,
        reg_stats=reg_manager.get_stats(),
        category_names=get_category_names(),
    )


def do_workspace_post(workspace, action):
    reserved = {x.name: x for x in workspace.files if x.name in RESERVED_FILE_NAMES}
    if action == "update_config":
        load_categories_config(reserved["config"].file_id)
        return render_template(
            "success.html", message="Конфигурационный файл был обновлён"
        )
    elif action == "upload_reg":
        file_id = reserved.get("reg") and reserved["reg"].file_id
        input_url = request.form["url"].strip()
        if input_url:
            file_id = utils.get_file_id_from_url(input_url)
        if file_id is None:
            raise WrongDataFormatError(
                f"'{input_url}' не выглядит как ссылка на Google Sheets"
            )
        reg_manager.load_registration_table(file_id, workspace)
        return render_template("success.html", message="Список участников был обновлён")
    elif action == "create_round":
        ckey = request.form["ckey"]
        stage = int(request.form["stage"])
        participants = list(map(int, request.form["participants"].split(",")))
        next_participants_count = int(request.form["next_participants_count"])
        heats_count = int(request.form["heats_count"])
        round_manager.gen_round(
            workspace=workspace,
            ckey=ckey,
            stage=stage,
            participants=participants,
            next_participants_count=next_participants_count,
            heats_count=heats_count,
        )
        return "<h1>hi</h1>"
    elif action == "process_final":
        ckey = request.form["ckey"]
        judges_count = int(request.form["judges_count"])
        dances_count = int(request.form["dances_count"])
        round_manager.process_final(
            workspace, ckey, judges_count=judges_count, dances_count=dances_count
        )
        return "<h1>hi</h1>"
    raise ApplicationError("Unknown action", f"Got unknown action '{action}'")


@index.route("/workspaces/<workspace_id>/stages", methods=["GET"])
def workspaces_stages(workspace_id: str):
    ckey = request.args.get("ckey")
    if not ckey:
        return render_template("stages_response.html", items={})
    workspace = retrieve_workspace(workspace_id)
    stages = round_manager.get_round_stages(workspace, ckey)
    m = {s: round_manager.round2name(s) for s in stages}
    return render_template("stages_response.html", items=m)


@index.route("/workspaces/<workspace_id>/participants", methods=["GET"])
def workspaces_participants(workspace_id: str):
    ckey = request.args.get("ckey")
    stage = request.args.get("stage")
    if not ckey or not stage:
        return "<h6>Ошибка</h6>"
    workspace = retrieve_workspace(workspace_id)
    participants = round_manager.get_round_participants(workspace, ckey, int(stage))
    return render_template("round_participants.html", p=participants)


@index.route("/workspaces/<workspace_id>/finals", methods=["GET"])
def workspaces_finals(workspace_id: str):
    ckey = request.args.get("ckey")
    retrieve_workspace(workspace_id)  # implicit
    stats = round_manager.get_category_stats(ckey)
    return render_template(
        "finals_data_response.html",
        judges_count=stats["judges_count"],
        dances_count=stats["dances_count"],
    )
