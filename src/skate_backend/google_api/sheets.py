from googleapiclient.http import MediaFileUpload

from skate_backend.google import google_manager


XLSX_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"


def create_sheet(spreadsheet):
    file = google_manager.sheets.create(body=spreadsheet).execute()
    return file.get("spreadsheetId")


def upload_sheet(file, name):
    file_metadata = dict(name=name, mimeType="application/vnd.google-apps.spreadsheet")
    media = MediaFileUpload(file, mimetype=XLSX_MIME_TYPE)

    file = google_manager.drive.create(
        body=file_metadata, media_body=media, fields="id"
    ).execute()
    return file.get("id")


def get_sheet(file_id: str, range: str):
    response = (
        google_manager.sheets.values()
        .get(
            spreadsheetId=file_id,
            range=range,
            majorDimension="ROWS",
            valueRenderOption="FORMATTED_VALUE",
        )
        .execute()
    )
    return response.get("values", [])


def update_sheet(
    file_id: str,
    range: str,
    values: list[list],
    value_input_option: str = "USER_ENTERED",
):
    google_manager.sheets.values().update(
        spreadsheetId=file_id,
        range=range,
        valueInputOption=value_input_option,
        body=dict(values=values),
    ).execute()


def clear_sheet(file_id: str, range: str):
    google_manager.sheets.values().clear(spreadsheetId=file_id, range=range).execute()
