from skate_backend.google import google_manager


def grant_permissions(file_id: str, email: str, role: str):
    google_manager.permissions.create(
        fileId=file_id,
        body=dict(type="user", emailAddress=email, role=role),
    ).execute()
