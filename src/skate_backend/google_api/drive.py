from googleapiclient.http import MediaFileUpload

from skate_backend.google import google_manager


def create_folder(name: str):
    file_metadata = {
        "name": name,
        "mimeType": "application/vnd.google-apps.folder",
    }
    file = google_manager.drive.create(body=file_metadata, fields="id").execute()
    return file.get("id")


def update_file(
    file_id: str,
    add_parents: list[str] | None = None,
    remove_parents: list[str] | None = None,
    filename: str | None = None,
    mimetype: str = "",
    name: str | None = None,
):
    kwargs = {"fileId": file_id}

    def add(cond, name, b):
        if cond:
            kwargs[name] = b

    body = {}
    if name:
        body["name"] = name

    media = MediaFileUpload(filename, mimetype=mimetype) if filename else None

    add(body, "body", body)
    add(add_parents, "addParents", ",".join(add_parents or []))
    add(remove_parents, "removeParents", ",".join(remove_parents or []))
    add(media, "media_body", media)

    google_manager.drive.update(**kwargs).execute()
