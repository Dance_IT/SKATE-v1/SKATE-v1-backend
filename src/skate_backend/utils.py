import collections
import re


def nested_dict():
    return collections.defaultdict(nested_dict)


def get_file_id_from_url(url: str) -> str | None:
    match = re.search(r"/d/([^/]+)", url)
    if match:
        return match.group(1)


def extract_file_id(url_or_file_id: str) -> str:
    maybe_parsed = get_file_id_from_url(url_or_file_id)
    file_id = maybe_parsed if maybe_parsed else url_or_file_id
    return file_id


def extract_sheet_data(data, ix=0):
    data = data["data"][ix].get("rowData", [])
    result = [[] for _ in range(len(data))]
    for i, row in enumerate(data):
        for col in row.get("values", []):
            result[i].append(col.get("formattedValue"))

    return result


def cell_is_empty(cell):
    return cell is None or len(str(cell).strip()) == 0


def default_str(s):
    return str(s) if s is not None else ""


def first(lst, pred):
    return next((x for x in lst if pred(x)), None)


_subscripts = [chr(x) for x in range(0x2080, 0x2089 + 1)]


def subscript_digit(x):
    return _subscripts[x]
