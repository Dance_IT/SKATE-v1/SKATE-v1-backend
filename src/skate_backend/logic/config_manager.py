import json

from pathlib import Path

from googleapiclient.errors import HttpError

from skate_backend.config import Config
from skate_backend.ctx import WorkspaceName
from skate_backend.exceptions import ApplicationError
from skate_backend.exceptions import GoogleAPIError
from skate_backend.exceptions import WrongDataFormatError
from skate_backend.google import google_manager
from skate_backend.types import JsonType
from skate_backend.utils import cell_is_empty
from skate_backend.utils import default_str
from skate_backend.utils import extract_sheet_data
from skate_backend.utils import nested_dict


_CONFIG_NAME = "config.json"
_REG_FILENAME = "reg.csv"


def _patch_config(config: dict[str, JsonType], path: Path) -> dict[str, JsonType]:
    d = nested_dict()
    d |= config
    d["registration"]["results_path"] = str(path.parent / _REG_FILENAME)
    if "registration" not in config or "start_no_column" not in config["registration"]:
        d["registration"]["start_no_column"] = "#"
    if "registration" not in config or "meta_columns" not in config["registration"]:
        d["registration"]["meta_columns"] = [
            "Партнёр",
            "Партнёрша",
            "Коллектив",
            "Тренеры",
        ]
    if (
        "registration" not in config
        or "categories_column" not in config["registration"]
    ):
        d["registration"]["categories_column"] = "Категории"
    return d


def get_workspace_path() -> Path:
    return Config.WORKDIR / WorkspaceName.get()


def get_config_path() -> Path:
    return get_workspace_path() / _CONFIG_NAME


def get_config() -> JsonType:
    path = get_config_path()
    if not path.is_file():
        put_config(dict(categories=[]))
    with path.open("r", encoding="utf-8") as f:
        data: JsonType = json.load(f)
    return data


def put_config(config: dict[str, JsonType]) -> JsonType:
    path = get_config_path()
    config = _patch_config(config, path)
    with path.open("w", encoding="utf-8") as f:
        json.dump(config, f, ensure_ascii=False)
    return config


def _parse_dances(dances: str) -> list[str]:
    return [x.strip() for x in dances.split(",")]


def _parse_categories(categories_data):
    if categories_data[0][0].strip() in ["ID", "Ключ"]:
        categories_data.pop(0)

    for c in categories_data:
        if len(c) != 3:
            raise ApplicationError(
                "Categories sheet should have exactly 3 columns: ID, name, dances"
            )

    return {
        id: dict(dances=_parse_dances(dances), name=name.strip(), judges=[])
        for id, name, dances in categories_data
    }


def _parse_judges(judges_data):
    if not judges_data:
        raise WrongDataFormatError("Неверный формат данных в таблице 'Judges'")
    category_names = [default_str(name).strip() for name in judges_data[0][1:]]
    data = [[] for _ in range(len(category_names))]
    for row in judges_data[1:]:
        if len(row) == 0 or row[0] is None:
            continue
        judge_name = row[0].strip()
        for i, col in enumerate(row[1:]):
            if cell_is_empty(col) or i >= len(data):
                continue
            data[i].append(judge_name)

    return {c: judges for c, judges in zip(category_names, data) if c.strip()}


def _parse_categories_judges(sheets):
    if len(sheets) != 2:
        raise ApplicationError(
            "The document should have exactly two sheets: categories and judges"
        )
    categories_data = extract_sheet_data(sheets[0])
    judges_data = extract_sheet_data(sheets[1])

    if cell_is_empty(categories_data[0][0]):
        categories_data, judges_data = judges_data, categories_data

    categories = _parse_categories(categories_data)
    reverse_category_index = {v["name"]: k for k, v in categories.items()}
    judges = _parse_judges(judges_data)
    for category_name_or_id, lst in judges.items():
        category_id = category_name_or_id
        if category_name_or_id not in categories:
            category_id = reverse_category_index[category_name_or_id]
        categories[category_id]["judges"] = lst
    return categories


def load_categories_config(spreadsheet_id: str):
    config = get_config()
    try:
        result = google_manager.sheets.get(
            spreadsheetId=spreadsheet_id,
            fields="sheets.data.rowData.values.formattedValue",
        ).execute()
    except HttpError as err:
        raise GoogleAPIError(
            f"Unknown HttpError while fetching data"
            f" from spreadsheet [{spreadsheet_id = }], status = {err.resp.status}"
        )
    print(result)
    categories = _parse_categories_judges(result["sheets"])
    config["categories"] = categories
    return put_config(config)
