import tanskate
import tanskate.registration

from pydantic import ValidationError

from skate_backend.logic.config_manager import get_config_path


def get_category_names() -> dict[str, str]:
    config_path = get_config_path()
    if not config_path.is_file():
        return {}
    try:
        config = tanskate.config.CompetitionConfig.load(config_path)
        return {k: v.name for k, v in config.categories.items()}
    except ValidationError:
        return {}
