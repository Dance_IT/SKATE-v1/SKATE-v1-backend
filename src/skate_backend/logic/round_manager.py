import itertools
import tempfile

from typing import NamedTuple

import tanskate
import tanskate.registration

from tanskate import count_final as tanskate_count_final
from tanskate.gen_round import gen_round as tanskate_gen_round

from skate_backend.db import Round
from skate_backend.db import Workspace
from skate_backend.db import db
from skate_backend.exceptions import ApplicationError
from skate_backend.google_api import drive
from skate_backend.google_api import sheets
from skate_backend.logic.config_manager import get_config_path
from skate_backend.logic.config_manager import get_workspace_path
from skate_backend.utils import subscript_digit


def round2name(x: int) -> str:
    return "\u00b9\u2044" + "".join([subscript_digit(int(i)) for i in str(1 << x)])


def pretty_round2name(x: int) -> str:
    if x == 0:
        return "финал"
    return f"{round2name(x)} финала"


max_round_stage = 6


def get_round_stages(workspace: Workspace, ckey: str):
    rounds = (
        db.session.query(Round).filter_by(workspace_id=workspace.id, ckey=ckey).all()
    )
    start_from = max(min([x.stage for x in rounds], default=0) - 1, 0)
    available_stages = list(range(start_from, max_round_stage + 1))
    return available_stages


class RoundParticipant(NamedTuple):
    id: int
    score: int
    passes_to_next_round: bool


class RoundParticipants(NamedTuple):
    is_first_round: bool
    participants: list[RoundParticipant]


# Таблица № 14
# Максимальное количество пар в туре
MAX_PARTICIPANTS_COUNT = {0: 8, 1: 15, 2: 30, 3: 60, 4: 120, 5: 240, 6: 480}


def _select_participants(
    raw_participants: list[tuple[int, int]], limit: int, stage: int
) -> list[RoundParticipant]:
    raw_participants = sorted(raw_participants, key=lambda x: (-x[1], x[0]))
    groups = [
        list(g) for _, g in itertools.groupby(raw_participants, key=lambda x: x[1])
    ]
    ret = []
    max_participants_count = MAX_PARTICIPANTS_COUNT[stage]
    stop = False
    for g in groups:
        if stop:
            passes_to_next_round = False
        elif len(ret) + len(g) <= limit:
            passes_to_next_round = True
        elif len(ret) < limit:  # ситуация, когда несколько пар делят последнее место
            passes_to_next_round = len(ret) + len(g) <= max_participants_count
            stop = True
        else:
            passes_to_next_round = False
        ret.extend(
            [
                RoundParticipant(participant_id, score, passes_to_next_round)
                for participant_id, score in g
            ]
        )
    return ret


def get_round_participants(
    workspace: Workspace, ckey: str, stage: int
) -> RoundParticipants:
    prev_round = (
        db.session.query(Round)
        .filter_by(workspace_id=workspace.id, ckey=ckey, stage=stage + 1)
        .one_or_none()
    )
    if not prev_round:
        config_path = get_config_path()
        config = tanskate.config.CompetitionConfig.load(config_path)
        reg_results = tanskate.registration.RegistrationResults.load(
            config.registration
        )
        reg_entries = reg_results.category_entries(ckey)
        return RoundParticipants(
            is_first_round=True,
            participants=[
                RoundParticipant(entry.start_no, 0, True) for entry in reg_entries
            ],
        )
    data = sheets.get_sheet(prev_round.file_id, "'Протокол'")
    result_col_idx = data[0].index("Сумма")
    results = [
        (int(row[0]), int(row[result_col_idx])) for row in data[2:] if row and row[0]
    ]
    return RoundParticipants(
        is_first_round=False,
        participants=_select_participants(
            results, prev_round.next_participants_count, stage
        ),
    )


def gen_round(
    workspace: Workspace,
    ckey: str,
    stage: int,
    participants: list[int],
    next_participants_count: int,
    heats_count: int,
):
    config_path = get_config_path()
    config = tanskate.config.CompetitionConfig.load(config_path)

    out_fname = get_workspace_path() / f"{ckey}#{stage}.xlsx"
    tanskate_round_name = pretty_round2name(stage)
    with tempfile.NamedTemporaryFile(mode="w", delete_on_close=False) as fp:
        fp.write("\n".join(map(str, participants)))
        fp.close()
        tanskate_gen_round(
            config=config_path,
            category_key=ckey,
            round_name=tanskate_round_name,
            participants_file=fp.name,
            out_workbook=out_fname,
            choose=next_participants_count,
            heats_count=heats_count,
        )
    round = (
        db.session.query(Round)
        .filter_by(workspace_id=workspace.id, ckey=ckey, stage=stage)
        .one_or_none()
    )
    round_name = f"{config.categories[ckey].name} {round2name(stage)}"

    if not round:
        file_id = sheets.upload_sheet(out_fname, round_name)
        drive.update_file(file_id, add_parents=[workspace.file_id])
        round = Round(
            workspace_id=workspace.id,
            ckey=ckey,
            stage=stage,
            file_id=file_id,
            participants_count=len(participants),
            next_participants_count=next_participants_count,
            heats_count=heats_count,
        )
        db.session.add(round)
        db.session.commit()
    else:
        drive.update_file(
            round.file_id,
            filename=out_fname,
            mimetype=sheets.XLSX_MIME_TYPE,
            name=round_name,
        )
        round.participants_count = len(participants)
        round.next_participants_count = next_participants_count
        round.heats_count = heats_count
        db.session.commit()


def get_category_stats(ckey: str) -> dict:
    config_path = get_config_path()
    config = tanskate.config.CompetitionConfig.load(config_path)
    category = config.categories[ckey]
    return dict(dances_count=len(category.dances), judges_count=len(category.judges))


def _calc_final_results(
    table: list[list[str]], dances_count: int, judges_count: int
) -> list[list[str]]:
    # https://gitlab.com/kb_ballroom/tanskate/-/blob/a211774ad1195b7acfce80d81b4b8006320a7f83/src/tanskate/count_final.py#L362-375
    dance_marks, unused_judges_count = tanskate_count_final.parse_table(
        table, dances_count, judges_count, 0
    )
    full_ranks = tanskate_count_final.calculate_full_ranks(dance_marks)
    return tanskate_count_final.gen_table(
        dance_marks,
        unused_judges_count,
        full_ranks,
    )


def _build_final_table(results: list[tuple[int, int]]) -> list[list]:
    config_path = get_config_path()
    config = tanskate.config.CompetitionConfig.load(config_path)
    reg_results = tanskate.registration.RegistrationResults.load(config.registration)
    entry_ids = [r[0] for r in results]
    ranks = [r[1] for r in results]

    entries = reg_results.find_entries(entry_ids)

    res = [["Место", "#", *config.registration.meta_columns]]
    for rank, id, entry in zip(ranks, entry_ids, entries):
        res.append([rank, id, *entry.meta])
    return res


def _parse_results_table(raw_data):
    data = []
    ix = 0

    def skip_empty_rows(ix):
        while ix < len(raw_data) and (not raw_data[ix] or not raw_data[ix][0]):
            ix += 1
        return ix

    while True:
        ix = skip_empty_rows(ix)
        if ix >= len(raw_data):
            break
        name = raw_data[ix][0]
        ix += 1
        ix = skip_empty_rows(ix)
        if ix >= len(raw_data):
            break
        content = []
        while ix < len(raw_data) and raw_data[ix]:
            content.append(raw_data[ix])
            ix += 1
        data.append((name, content))
    return data


def _build_results_table(results: list[tuple[str, list[list]]]) -> list[list]:
    ret = []
    for i, (name, content) in enumerate(results):
        ret.append([name])
        ret.append([])
        ret.append([])
        ret.extend(content)
        if i != len(results) - 1:
            ret.append([])
            ret.append([])
            ret.append([])
    return ret


def _rebuild_results_table(file_id: str, ckey: str, ckey_results: list[list[str]]):
    raw_data = sheets.get_sheet(file_id, "'Награждение'")
    results = _parse_results_table(raw_data)

    config_path = get_config_path()
    config = tanskate.config.CompetitionConfig.load(config_path)
    category_name = config.categories[ckey].name

    for i, (name, _) in enumerate(results):
        if name == category_name:
            ix = i
            break
    else:
        ix = None

    if ix:
        results[ix] = (category_name, ckey_results)
    else:
        results.append((category_name, ckey_results))

    updated_sheet = _build_results_table(results)
    sheets.clear_sheet(file_id, "'Награждение'")
    sheets.update_sheet(
        file_id, "'Награждение'", updated_sheet, value_input_option="RAW"
    )


def process_final(
    workspace: Workspace, ckey: str, judges_count: int, dances_count: int
):
    round = (
        db.session.query(Round)
        .filter_by(workspace_id=workspace.id, ckey=ckey, stage=0)
        .one_or_none()
    )
    if not round:
        raise ApplicationError(
            f"Для {ckey} не проводилось финала", "Проведите сначала финал"
        )

    data = sheets.get_sheet(round.file_id, "'Протокол'")
    table = [
        row[: 1 + judges_count * dances_count] for row in data[2:] if row and row[0]
    ]
    final_results = _calc_final_results(
        table, dances_count=dances_count, judges_count=judges_count
    )

    sheets.update_sheet(round.file_id, "'Протокол'!A3", final_results)

    final_table = _build_final_table(
        [(int(row[0]), int(row[-1])) for row in final_results]
    )
    results_file_id = next(f.file_id for f in workspace.files if f.name == "results")
    _rebuild_results_table(results_file_id, ckey, final_table)
