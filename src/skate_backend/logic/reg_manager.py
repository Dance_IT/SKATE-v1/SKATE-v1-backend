import csv
import io
import logging

from pathlib import Path

import tanskate
import tanskate.registration

from googleapiclient.errors import HttpError
from googleapiclient.http import MediaIoBaseDownload
from pydantic import ValidationError

from skate_backend.db import File
from skate_backend.db import Workspace
from skate_backend.db import db
from skate_backend.exceptions import ApplicationError
from skate_backend.google import google_manager
from skate_backend.logic.config_manager import get_config
from skate_backend.logic.config_manager import get_config_path


def _download_file(file_id: str) -> bytes:
    request = google_manager.drive.export_media(fileId=file_id, mimeType="text/csv")
    file = io.BytesIO()
    downloader = MediaIoBaseDownload(file, request)
    done = False
    while done is False:
        _, done = downloader.next_chunk()
    return file.getvalue()


def load_registration_table(file_id: str, workspace: Workspace):
    config = get_config()
    reg_file = Path(config["registration"]["results_path"])

    try:
        data = _download_file(file_id)
    except HttpError as err:
        logging.exception(f"Got HttpError while downloading file [{file_id = }]")
        raise ApplicationError(
            f"Unknown HttpError while downloading file [{file_id = }],"
            f" status = {err.resp.status}"
        )

    logging.info(f"Downloaded file [{file_id = }]: {len(data)} bytes")

    with reg_file.open("wb") as f:
        f.write(data)

    # upsert
    for f in workspace.files:
        if f.name == "reg":
            f.file_id = file_id
            break
    else:
        workspace.files.append(File(name="reg", file_id=file_id))
    db.session.add(workspace)
    db.session.commit()

    ids = []
    rows_count = 0
    id_col_name = config["registration"]["start_no_column"]
    with reg_file.open("r", newline="", encoding="utf-8") as f:
        reader = csv.DictReader(f)
        for row in reader:
            raw_id = row[id_col_name].strip()
            if raw_id:
                ids.append(int(raw_id))
            rows_count += 1
    return {"rows_count": rows_count, "ids_count": len(ids), "ids": ids}


def get_stats() -> dict[str, int] | None:
    config_path = get_config_path()
    if not config_path.is_file():
        return
    try:
        config = tanskate.config.CompetitionConfig.load(config_path)
    except ValidationError:
        print('here')
        return
    reg_results = tanskate.registration.RegistrationResults.load(config.registration)
    counters = reg_results.category_participants_count()
    res = {}
    for category_key, category in config.categories.items():
        count = counters.get(category_key, 0)
        res[category.name] = count
    return res
