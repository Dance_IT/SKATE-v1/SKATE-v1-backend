import logging

from googleapiclient.errors import HttpError

from skate_backend.config import Config
from skate_backend.db import File
from skate_backend.db import Workspace
from skate_backend.db import db
from skate_backend.exceptions import ApplicationError
from skate_backend.exceptions import GoogleAPIError
from skate_backend.google_api import drive
from skate_backend.google_api import permissions
from skate_backend.google_api import sheets


logger = logging.getLogger(__name__)

RESERVED_FILE_NAMES = ["config", "reg"]


def _create_drive(name: str) -> str:
    file_id = drive.create_folder(name)
    permissions.grant_permissions(file_id, Config.CREATOR_EMAIL, "writer")
    return file_id


def _prepare_sheet_payload(data: list[list[str]]):
    return [
        dict(values=[dict(userEnteredValue=dict(stringValue=x)) for x in row])
        for row in data
    ]


def _create_config_sheet(folder_id: str) -> str:
    file_id = sheets.create_sheet(
        {
            "properties": {"title": "Config"},
            "sheets": [
                {
                    "properties": {"title": "Config"},
                    "data": [
                        {
                            "startRow": 0,
                            "startColumn": 0,
                            "rowData": _prepare_sheet_payload(
                                [["Ключ", "Имя", "Танцы"]]
                            ),
                        }
                    ],
                },
                {"properties": {"title": "Judges"}},
            ],
        }
    )
    drive.update_file(file_id, add_parents=[folder_id], remove_parents=["root"])
    return file_id


def _create_results_sheet(folder_id: str) -> str:
    file_id = sheets.create_sheet(
        {
            "properties": {"title": "Награждение"},
            "sheets": [{"properties": {"title": "Награждение"}}],
        }
    )
    drive.update_file(file_id, add_parents=[folder_id], remove_parents=["root"])
    return file_id


def create_workspace(name: str):
    path = Config.WORKDIR / name

    try:
        path.mkdir(parents=True)
        (path / "reg.csv").touch()
    except FileExistsError:
        raise ApplicationError(
            "Workspace already exists!",
            f"Workspace `{name}` already exists [{path = }]",
        )

    try:
        folder_id = _create_drive(name)
    except HttpError as err:
        raise GoogleAPIError(
            f"Error while creating Google Drive. Status = {err.status_code}, reason = {err.reason}"
        )
    logger.info(f"Created folder [{folder_id = }]")

    try:
        config_id = _create_config_sheet(folder_id)
    except HttpError as err:
        raise GoogleAPIError(
            f"Error while creating config sheet. Status = {err.status_code}, reason = {err.reason}"
        )
    logger.info(f"Created config [{config_id = }]")

    try:
        results_id = _create_results_sheet(folder_id)
    except HttpError as err:
        raise GoogleAPIError(
            f"Error while creating results sheet. Status = {err.status_code}, reason = {err.reason}"
        )
    logger.info(f"Created results [{results_id = }]")

    workspace = Workspace(
        name=name,
        file_id=folder_id,
        files=[
            File(name="config", file_id=config_id),
            File(name="results", file_id=results_id),
        ],
    )
    db.session.add(workspace)
    db.session.commit()


def list_workspaces() -> list:
    return [
        dict(id=x.id, name=x.name)
        for x in db.session.execute(db.select(Workspace)).scalars()
    ]


def get_workspace_by_id(id: int) -> Workspace | None:
    return db.session.get(Workspace, id)
