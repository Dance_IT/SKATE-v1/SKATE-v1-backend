function participants_selector_register() {
    const participants_raw = document.getElementById("participants_raw");
    const participants_count = document.getElementById("participants_count");
    console.log(participants_raw);
    const table = document.getElementById("participants_table");
    function update() {
        const rows = table.getElementsByTagName('tbody')[0].getElementsByTagName('tr');
        let res = [];
        for (let i = 0; i < rows.length; i++) {
            const columns = rows[i].getElementsByTagName('td');
            console.log(columns[2].getElementsByTagName("input")[0].checked, columns[0].textContent);
            if (columns[2].getElementsByTagName("input")[0].checked) {
                res.push(columns[0].textContent);
            }
        }
        participants_raw.setAttribute("value", res.join(","));
        participants_count.textContent = res.length;
    }
    update();
    table.querySelectorAll('input[type="checkbox"]').forEach(node => {
        console.log(node);
        node.addEventListener('change', update);
    });
}