import os

from pathlib import Path


class Config:
    WORKDIR = Path(os.getenv("SKATE_WORKDIR", "."))
    DB_URL = os.getenv("SKATE_DB_URI")
    CREATOR_EMAIL = os.getenv("SKATE_CREATOR_EMAIL")
    GOOGLE_SERVICE_ACCOUNT_FILE = os.getenv("SKATE_SERVICE_ACCOUNT_FILE")
