from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.orm import relationship

from skate_backend.db._db import db


class Workspace(db.Model):  # type: ignore[name-defined,misc]
    __tablename__ = "workspaces"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(unique=True, nullable=False)
    file_id: Mapped[str] = mapped_column(unique=True, nullable=False)
    files: Mapped[list["File"]] = relationship()


class File(db.Model):  # type: ignore[name-defined,misc]
    __tablename__ = "files"
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(nullable=False)
    file_id: Mapped[str] = mapped_column(nullable=False)
    workspace_id: Mapped[int] = mapped_column(
        ForeignKey("workspaces.id"), nullable=False
    )


class Round(db.Model):  # type: ignore[name-defined,misc]
    __tablename__ = "rounds"
    workspace_id: Mapped[int] = mapped_column(primary_key=True)
    ckey: Mapped[str] = mapped_column(primary_key=True)
    stage: Mapped[int] = mapped_column(primary_key=True)
    file_id: Mapped[str] = mapped_column(nullable=False)
    participants_count: Mapped[int] = mapped_column(nullable=False)
    next_participants_count: Mapped[int] = mapped_column(nullable=False)
    heats_count: Mapped[int] = mapped_column(nullable=False)
