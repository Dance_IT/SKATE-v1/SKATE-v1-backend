import logging
import os

from typing import Any

from flask import Flask
from flask import render_template
from flask_wtf.csrf import CSRFProtect

from skate_backend.api.index import index
from skate_backend.config import Config
from skate_backend.db import db
from skate_backend.exceptions import ApplicationError
from skate_backend.google import google_manager


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s",
)

csrf = CSRFProtect()


def create_app() -> Flask:
    app = Flask(__name__, static_folder="static")
    app.logger.setLevel(logging.DEBUG)

    @app.errorhandler(ApplicationError)
    def _(e: ApplicationError) -> Any:
        logging.exception("Got exception")
        return render_template("error.html", title=e.title, message=e.message)

    google_manager.init()
    app.config["SQLALCHEMY_DATABASE_URI"] = Config.DB_URL
    app.secret_key = os.getenv("SECRET_KEY")
    db.init_app(app)
    csrf.init_app(app)

    app.register_blueprint(index)

    return app


def main() -> None:
    app = create_app()
    app.run(debug=True)


if __name__ == "__main__":
    main()
