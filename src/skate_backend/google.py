from typing import Any

from google.oauth2 import service_account
from googleapiclient.discovery import build  # type: ignore[import-untyped]

from skate_backend.config import Config


class _GoogleManager:
    def __init__(self) -> None:
        self.sheets: Any = None
        self.drive: Any = None

    def init(self) -> None:
        _credentials = service_account.Credentials.from_service_account_file(  # type: ignore[no-untyped-call]
            Config.GOOGLE_SERVICE_ACCOUNT_FILE,
            scopes=[
                "https://www.googleapis.com/auth/spreadsheets",
                "https://www.googleapis.com/auth/drive",
            ],
        )

        self.sheets = build("sheets", "v4", credentials=_credentials).spreadsheets()
        self.drive = build("drive", "v3", credentials=_credentials).files()
        self.permissions = build("drive", "v3", credentials=_credentials).permissions()


google_manager = _GoogleManager()
