import json
import sys
import typing

from pathlib import Path

from openpyxl import load_workbook

from skate_backend.logic import round_manager


def _parse_title(name: str) -> tuple[str, int]:
    a, b = name.split(",")

    return a.strip(), _parse_stage(b.strip())


_stages = {0: "финал", 1: "\u00bd финала", 2: "\u00bc финала", 3: "\u215b финала"}


def _parse_stage(n: str) -> int:
    for i in range(8):
        if (
            round_manager.pretty_round2name(i) == n.lower()
            or _stages.get(i) == n.lower()
        ):
            return i


def _parse_judges(wb):
    idx = 4
    ws = wb["Сводка"]
    lst = []
    while True:
        if not ws.cell(row=idx, column=1).value:
            break
        lst.append(ws.cell(row=idx, column=2).value.strip())
        idx += 1
    return lst


def _parse_dances(wb):
    ws = wb["Бланк"]
    cnt = 1
    for row in ws.iter_rows(min_row=cnt):
        if not row:
            break
        cnt += 1
    row_number = cnt + 1
    dances = [
        x.value
        for x in next(ws.iter_rows(min_row=row_number, max_row=row_number))
        if x.value
    ]
    return dances


class ResultRow(typing.NamedTuple):
    participant_id: int
    raw_data: list[str]
    dance_places: list[str]
    score: int


class Round(typing.NamedTuple):
    name: str
    stage: int
    judges: list[str]
    dances: list[str]
    results: list[ResultRow]


def _parse_result(wb, judges_count, dances_count, is_final):
    ws = wb["Протокол"]
    result = []
    raw_data_len = (
        judges_count * dances_count + dances_count
        if is_final
        else judges_count * dances_count
    )
    for row in ws.iter_rows(min_row=3):
        if not row or row[0].value is None:
            break
        id = int(row[0].value)
        raw_data = [x.value or "" for x in row[1 : raw_data_len + 1]]
        score = row[raw_data_len + 1].value
        dance_places = []
        if is_final:
            raw_data, dance_places = raw_data[:-dances_count], raw_data[-dances_count:]
        result.append(ResultRow(id, raw_data, dance_places, score))
    return result


def do_export(dir, out):
    lst = []
    for p in Path(dir).iterdir():
        print(f"Processing {p}")
        wb = load_workbook(filename=p, read_only=True, data_only=True)

        name, stage = _parse_title(wb["Сводка"].cell(row=1, column=1).value)
        if stage is None:
            print("error: not a valid stage")
            exit(1)

        judges = _parse_judges(wb)
        dances = _parse_dances(wb)
        results = _parse_result(wb, len(judges), len(dances), stage == 0)
        lst.append(Round(name, stage, judges, dances, results))
        lst[-1] = lst[-1]._asdict()
        lst[-1]["results"] = [x._asdict() for x in lst[-1]["results"]]

    with open(out, "w") as f:
        json.dump(lst, f, ensure_ascii=False)


def main():
    if len(sys.argv) < 2:
        print("usage: <program> <dir> <out.json>")
        exit(1)

    if len(sys.argv) >= 3:
        do_export(sys.argv[1], sys.argv[2])


if __name__ == "__main__":
    main()
