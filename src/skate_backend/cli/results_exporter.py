import csv
import itertools
import json
import sys


header = r"""\documentclass[a4paper,10pt]{article}

\usepackage{tabularx,ragged2e,booktabs,caption,tabularray}
\usepackage{longtable}

\usepackage{phypreamble}

\parindent=0pt
\newcolumntype{O}{>{\centering}p{0.02\textwidth}}
\newcolumntype{Q}{>{\centering}p{0.04\textwidth}}
\newcolumntype{D}{>{\centering}p{0.03\textwidth}}
\newcolumntype{P}{>{\centering}p{0.06\textwidth}}
\newcolumntype{R}{>{\centering}p{0.04\textwidth}}
\begin{document}
"""
META_COLUMNS = ["Коллектив", "Тренеры"]


def export_latex(agg_file, categories_file, reg_csv, out_file):
    with open(agg_file) as f:
        agg = json.load(f)

    with open(categories_file) as f:
        categories = [x.strip() for x in f.read().split("\n") if x.strip() and not x.startswith('#')]

    with open(reg_csv, newline="") as f:
        registration = csv.DictReader(f)
        registration = {int(x["#"]): x for x in registration if x["#"]}
    # print(registration)

    print(f"Exporting {categories = }")
    outf = open(out_file, "w")
    outf.write(header)
    outf.write("\n")
    for c in categories:
        export_latex_category(outf, [x for x in agg if x["name"] == c], registration)
        outf.write("\\clearpage\n")
    outf.write("\\end{document}\n")


def make_multiline(strs, align='l'):
    l = '\\\\'.join(strs)
    return f"\\begin{{tabular}}{{@{{}}{align}@{{}}}} {l} \\end{{tabular}}"


def export_latex_category(outf, agg, registration):
    places = get_places(agg)
    outf.write(f"\\section*{{{agg[0]['name']}}}\n")
    outf.write("\\subsection*{Результаты}\n")

    for stage in range(len(places)):
        outf.write(f"\\subsubsection*{{{_to_latex_stage(stage)}}}\n")
        columns = (
            ["P", "R", "p{4.2cm}"]
            + ["p{5.1cm}" for _ in range(len(META_COLUMNS))]
        )
        columns[-1] = "X"
        tbl_str = (
            f"\\begin{{tabularx}}{{\\textwidth}}{{|{'|'.join(columns)}|}} \\hline "
        )
        tbl_str += " & ".join(
            map(make_bold, ["Место", "\\#", "Участник", *META_COLUMNS])
        )
        tbl_str += "\\\\\\hline "
        for id, place in places[stage].items():
            place = str(place[0]) if place[0] == place[1] else f"{place[0]}--{place[1]}"
            names = [x for x in [registration[id]["Партнёр"], registration[id]["Партнёрша"]] if x.strip()]
            manual_meta_columns = []
            if 'Коллектив' in META_COLUMNS:
                raw = registration[id]['Коллектив']
                if len(names) == 1:
                    manual_meta_columns.append(raw)
                else:
                    manual_meta_columns.append(make_multiline([x.strip() for x in raw.split(',', 1) if x.strip()]))
            if 'Тренеры' in META_COLUMNS:
                raw = registration[id]['Тренеры']
                if len(names) == 1:
                    manual_meta_columns.append(raw)
                else:
                    manual_meta_columns.append(make_multiline([x.strip() for x in raw.split(',', 1) if x.strip()]))
            row = [place, str(id), make_multiline(names), *[registration[id][x] for x in META_COLUMNS if x not in set(['Тренеры', 'Коллектив'])], *manual_meta_columns]
            tbl_str += " & ".join(row)
            tbl_str += "\\\\\\hline "
        tbl_str += "\\end{tabularx}"

        outf.write(f"{{\\setlength{{\\tabcolsep}}{{3pt}}  {tbl_str}}}\n")
    outf.write("\\clearpage\n")
    outf.write("\\subsection*{Оценки}\n")
    judges = " ".join(f"\\item {x}" for x in agg[0]["judges"])
    judges_count = len(agg[0]["judges"])
    dances = agg[0]["dances"]
    outf.write(f"Судьи: \\begin{{enumerate}} {judges} \\end{{enumerate}}\n")
    agg_stages = {x["stage"]: x for x in agg}
    for stage in range(len(places)):
        outf.write(f"\\subsubsection*{{{_to_latex_stage(stage)}}}\n")
        is_final = stage == 0
        agg_sorted = sorted(
            agg_stages[stage]["results"],
            key=lambda x: _round_if_possible(x["score"]),
            reverse=not is_final,
        )
        if len(dances) >= 5:
            column_O = 'O'
        else:
            column_O = 'Q'
        dance_places_columns = column_O * len(agg_sorted[0]["dance_places"])
        if len(dances) >= 5:
            column_C_size = '3mm'
        else:
            column_C_size = '4mm'
        column_C = '@{}>{\\centering}p{%s}@{}' % column_C_size
        columns = [
            "D|",
            *[f"{column_C*judges_count}|" for _ in dances],
            f"{dance_places_columns}|" if dance_places_columns else "",
            "c",
        ]
        tbl_str = f"\\begin{{longtable}}[l]{{|{''.join(columns)}|}} \\hline "
        tbl_str += r"\multirow{2}{*}{\textbf{\#}} &"

        first_row = list(
            map(
                lambda x: f"\\multicolumn{{{judges_count}}}{{c|}}{{{make_bold(x)}}}",
                dances,
            )
        )
        if dance_places_columns:
            first_row.append(
                f"\\multicolumn{{{len(dance_places_columns)}}}{{c|}}{{{make_bold('Место в танце')}}}"
            )
        first_row.append(
            f"\\multirow{{2}}{{*}}{{{make_bold('Место' if is_final else 'Сумма')}}}"
        )
        tbl_str += " & ".join(first_row)

        cline_end = judges_count * len(dances) + len(dance_places_columns)
        tbl_str += f"\\\\ \\cline{{2-{cline_end+1}}} "
        second_row = [""]
        for _ in dances:
            second_row.extend([make_bold(str(x + 1)) for x in range(judges_count)])
            # second_row.extend([f"\\hskip 1mm \\relax {make_bold(str(x + 1))} \\hskip 1mm \\relax" for x in range(judges_count)])
        if dance_places_columns:
            second_row.extend([str(make_bold(x)) for x in dances])
        second_row.append("")
        tbl_str += " & ".join(second_row)
        tbl_str += "\\\\\\hline "
        last_idx = -1 if stage == 0 else len(agg_stages[stage - 1]["results"]) - 1
        for i, x in enumerate(agg_sorted):
            tmp = []
            tmp.append(str(x["participant_id"]))
            tmp.extend([str(_round_if_possible(b)) for b in x["raw_data"]])
            # tmp.extend([f"\\hskip 1pt \\relax {_round_if_possible(b)} \\hskip 1pt \\relax" for b in x["raw_data"]])
            tmp.extend([str(_round_if_possible(b)) for b in x["dance_places"]])
            tmp.append(str(_round_if_possible(x["score"])))
            tbl_str += " & ".join(tmp)
            if last_idx == i:
                tbl_str += "\\\\\\hline "
            else:
                tbl_str += "\\\\ "

        tbl_str += "\\hline \\end{longtable}"
        outf.write(f"{{\\setlength{{\\tabcolsep}}{{4pt}} {tbl_str}}}\n")


def make_bold(x):
    return f"\\textbf{{{x}}}"


def _to_latex_stage(n: int):
    if n == 0:
        return "Финал"
    return f"\\sfrac{{1}}{{{1 << n}}} финала"


def get_places(agg):
    agg_sorted = sorted(agg, key=lambda x: x["stage"])
    keys = set()
    lst = []
    place = 1
    for x in agg_sorted:
        # print(x)
        places = {}
        is_final = x["stage"] == 0
        for _, g in itertools.groupby(
            sorted(
                x["results"],
                key=lambda x: _round_if_possible(x["score"]),
                reverse=not is_final,
            ),
            key=lambda x: int(x["score"]),
        ):
            g = list([row for row in g if row["participant_id"] not in keys])
            end_place = place + len(g) - 1
            for row in g:
                places[row["participant_id"]] = (place, end_place)
                keys.add(row["participant_id"])
            place = end_place + 1
        lst.append(places)
    return lst


def _round_if_possible(x):
    try:
        if int(x) == x:
            return int(x)
        return x
    except ValueError:
        return x


def main():
    if len(sys.argv) < 6:
        print(
            "usage: <program> export_latex <agg.json> <categories.txt> <reg.csv> <out.tex>"
        )
        exit(1)

    if sys.argv[1] == "export_latex":
        export_latex(*sys.argv[2:])


if __name__ == "__main__":
    main()
