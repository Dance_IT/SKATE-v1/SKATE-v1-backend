check:
	poetry run ruff check

format:
	poetry run ruff format
	poetry run black src tests
